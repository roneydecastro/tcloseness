package examples;

import util.Config;
import util.Data;
import util.Hierarchy;
import util.Config.AttributeType;

public class InformationDataSet implements DataSet{

	
	public InformationDataSet(){
		
	}
	
	public Data createDataSet(){
		Data data = new Data();
        data.add("identifier", "name", "zip", "age", "nationality", "sen");
        data.add("a", "Alice", "47906", "35", "USA", "0");
        data.add("a", "Alice", "47906", "35", "USA", "0");
        data.add("a", "Alice", "47906", "35", "USA", "0");
        data.add("b", "Bob", "47903", "59", "Canada", "1");
        data.add("b", "Bob", "47903", "59", "Canada", "1");
        data.add("b", "Bob", "47903", "59", "Canada", "1");
        data.add("c", "Christine", "47906", "42", "USA", "1");
        data.add("c", "Christine", "47906", "42", "USA", "1");
        data.add("c", "Christine", "47906", "42", "USA", "1");
        data.add("d", "Dirk", "47630", "18", "Brazil", "0");
        data.add("d", "Dirk", "47630", "18", "Brazil", "0");
        //data.add("e", "Eunice", "47630", "22", "Brazil", "0");
        //data.add("f", "Frank", "47633", "63", "Peru", "1");
        //data.add("g", "Gail", "48973", "33", "Spain", "0");
        //data.add("h", "Harry", "48972", "47", "Bulgaria", "1");
        //data.add("i", "Iris", "48970", "52", "France", "1");

     
        return data;
		
	}
	
	public Config configDataSet(){
		   Hierarchy age = new Hierarchy();
	        age.add("18", "1*", "<=40", "*");
	        age.add("22", "2*", "<=40", "*");
	        age.add("33", "3*", "<=40", "*");
	        age.add("35", "3*", "<=40", "*");
	        age.add("42", "4*", ">40", "*");
	        age.add("47", "4*", ">40", "*");
	        age.add("52", "5*", ">40", "*");
	        age.add("59", "5*", ">40", "*");
	        age.add("63", "6*", ">40", "*");
	        
	        Hierarchy nationality = new Hierarchy();
	        nationality.add("Canada", "N. America", "America", "*");
	        nationality.add("USA", "N. America", "America", "*");
	        nationality.add("Peru", "S. America", "America", "*");
	        nationality.add("Brazil", "S. America", "America", "*");
	        nationality.add("Bulgaria", "E. Europe", "Europe", "*");
	        nationality.add("France", "W. Europe", "Europe", "*");
	        nationality.add("Spain", "W. Europe", "Europe", "*");

	        Hierarchy zip = new Hierarchy();
	        zip.add("47630", "4763*", "476*", "47*", "4*", "*");
	        zip.add("47633", "4763*", "476*", "47*", "4*", "*");
	        zip.add("47903", "4790*", "479*", "47*", "4*", "*");
	        zip.add("47906", "4790*", "479*", "47*", "4*", "*");
	        zip.add("48970", "4897*", "489*", "48*", "4*", "*");
	        zip.add("48972", "4897*", "489*", "48*", "4*", "*");
	        zip.add("48973", "4897*", "489*", "48*", "4*", "*");
	        
	        Config config = new Config();
	        config.addAttibuteType("identifier", AttributeType.IDENTIFYING_ATTRIBUTE);
	        config.addAttibuteType("name", AttributeType.SENSITIVE_ATTRIBUTE);
	        config.addAttibuteType("sen", AttributeType.IDENTIFYING_ATTRIBUTE);

	        config.setDefinitions("age", age);
	        config.addAttibuteType("age", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
	        
	        config.setDefinitions("nationality", nationality);
	        config.addAttibuteType("nationality", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);

	        config.setDefinitions("zip",zip);
	        config.addAttibuteType("zip", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
	        return config;
	}
	
}
