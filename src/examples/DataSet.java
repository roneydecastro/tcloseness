package examples;

import util.Config;
import util.Data;

public interface DataSet {

	public Data createDataSet();
	public Config configDataSet();
    //data.add("", "", "", "", "", "");

}
