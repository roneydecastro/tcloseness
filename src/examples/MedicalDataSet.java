package examples;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import util.Config;
import util.Data;
import util.Hierarchy;
import util.Print;
import util.Config.AttributeType;

public class MedicalDataSet  implements DataSet{
	

	public Data createDataSet(){
		Data data = new Data();
		data = importDataSetFromTxt("dataset.txt", ",");
     
        return data;
		
	}
	
	public Config configDataSet(){
		    //Education,Sex,Work,Disease,Salary
			Hierarchy sex = importHierarchyFromTxt("sexHier.csv", ";");
	        Hierarchy education = importHierarchyFromTxt("educationHier.csv", ";");
	        Hierarchy work = importHierarchyFromTxt("workHier.csv", ";");
	        //Hierarchy disease = importHierarchyFromTxt("diseaseHier.csv", ";");
	        Hierarchy user = importHierarchyFromTxt("userHier.csv", ";");

	        
	        Config config = new Config();
	        config.addAttibuteType("disease", AttributeType.IDENTIFYING_ATTRIBUTE);
	        config.addAttibuteType("salary", AttributeType.SENSITIVE_ATTRIBUTE);
	        //config.addAttibuteType("sen", AttributeType.IDENTIFYING_ATTRIBUTE);

	        config.setDefinitions("sex", sex);
	        config.addAttibuteType("sex", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
	        
	        config.setDefinitions("education", education);
	        config.addAttibuteType("education", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);

	        config.setDefinitions("work",work);
	        config.addAttibuteType("work", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
	        
	        config.setDefinitions("user",user);
	        config.addAttibuteType("user", AttributeType.QUASI_IDENTIFYING_ATTRIBUTE);
	        return config;
	}
	
	private Data importDataSetFromTxt(String source, String lineSeparator){
		Data data = new Data();
		
		try(BufferedReader br = new BufferedReader(new FileReader(source))) {
		    String line = br.readLine();

		    while (line != null) {
		    	data.add(line.split(lineSeparator));  
		        line = br.readLine();
		    }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}
	
	private Hierarchy importHierarchyFromTxt(String source, String lineSeparator){
		Hierarchy hierarchy = new Hierarchy();
		
		try(BufferedReader br = new BufferedReader(new FileReader(source))) {
		    String line = br.readLine();

		    while (line != null) {
		    	hierarchy.add(line.split(lineSeparator));  
		        line = br.readLine();
		    }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return hierarchy;
	}
	
}
