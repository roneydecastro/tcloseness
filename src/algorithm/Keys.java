package algorithm;

import java.util.Arrays;
import java.util.List;

public class Keys {

	String[] keys;
	public Keys(String[] keys) {
		super();
		this.keys = keys;
	}
	public Keys(String[] keys, List<Integer> indexes) {
		super();
		String[] newKeys= new String[keys.length];
		int cont = 0;
		for(int i=0;i<keys.length;i++){
			if(indexes.contains(i)){
				newKeys[cont] = keys[i];
				cont++;
			}
		}
		this.keys = newKeys;
	}
	@Override
	public String toString() {
		return Arrays.toString(keys);
	}
	
	
}
