package algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.Config;
import util.Data;
import util.Hierarchy;
import util.Config.AttributeType;
import util.Print;

public class Anonymizer {
	
		Config config;
		Data data;
		HashMap<String[],Integer> counter = new HashMap<String[],Integer>();
		List<Integer> indexes = new ArrayList<Integer>();

		
		public Anonymizer(Config config, Data data) {
			super();
			this.config = config;
			this.data = data;
		}



		public void anonymize(){
			Print.print(data);
			String[] columns = data.getData().remove(0);
			for(int i=0;i<columns.length;i++){
					if(isQID(columns[i])){
						indexes.add(i);
				}
			}
			System.out.println(indexes.toString());
			/*if(change(config.getDefinitions().get("age"), 3)){ //Change Age
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}
			if(change(config.getDefinitions().get("zip"), 2)){
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}
			if(change(config.getDefinitions().get("nationality"), 4)){
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}*/
			
			
			if(change(config.getDefinitions().get("sex"), 2)){
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}
			if(change(config.getDefinitions().get("education"), 1)){
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}
			if(change(config.getDefinitions().get("work"), 3)){
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}
			if(change(config.getDefinitions().get("user"), 0)){
				System.out.println("Is K Anonymity");
			}else{
				System.out.println("Is not K Anonymity");
			}
			
			
		}
		
		public boolean change(Hierarchy hierarchy, int hierarchyLevel){
			String key = " ";
			int level = 1;
			int keyLevel = 0;
			boolean flag = false;
			int sizeHierarchy = hierarchy.getHierarchy().get(0).length;
			while(!checkIsKAnonymity() && !flag){
				for(int i=0;i<data.getData().size();i++){
					String[] row = data.getData().get(i);
					String value = row[hierarchyLevel];
					
					for(String[] rowHierarchy:hierarchy.getHierarchy()){
						key = rowHierarchy[keyLevel];
						if(key.equals(value)){
							data.getData().get(i)[hierarchyLevel] = rowHierarchy[level];
						}
					}
				}
				level++;
				keyLevel++;
				Print.print(data);
				if(level>=sizeHierarchy){
					flag = true;
				}
					
			}
			if(flag==false){
				return true;
			}else{
				return false;
			}
			
		}
		
		
      
		public boolean checkIsKAnonymity(){
		
			Map<Keys,Integer> counter = new HashMap<Keys,Integer>();

			for(String[] instance:data.getData()){
				Keys keys = new Keys(instance, indexes);
				Keys k = hasKey(keys, counter);
				if(k!=null){
					counter.put(k, counter.get(k) + 1);
				}else{
					counter.put(keys,1);
				}
			}
			for(Keys key:counter.keySet()){
				if(counter.get(key)<((kAnonymity)config.getCriterion()).getK()){
					return false;
				}
			}
			
			Print.printHash(counter);
			
			return true;
			
		}
		
		private Keys hasKey(Keys keys, Map<Keys,Integer> counter){
		    for(Keys key : counter.keySet()){
		        if(key.toString().equals(keys.toString())){
		            return key;
		        }
		    }
		    return null;
		}
	

		
		private boolean isQID(String column){
			if(config.getAttibuteType(column)==AttributeType.QUASI_IDENTIFYING_ATTRIBUTE){
				return true;
			}
			
			return false;
		}
		
		
}
