package algorithm;


import examples.InformationDataSet;
import examples.MedicalDataSet;
import util.Config;
import util.Data;
import util.Print;
import util.Time;

public class Main {

	public static void main(String[] args) {
		Time time = new Time();
		time.start();
		
		//InformationDataSet set = new InformationDataSet();
		MedicalDataSet set = new MedicalDataSet();

        kAnonymity kAnonymity = new kAnonymity(3);

		Data data = set.createDataSet();
		Config config = set.configDataSet();
        config.addCriterion(kAnonymity);
        
        Anonymizer anonymizer = new Anonymizer(config, data);
        anonymizer.anonymize();
        
        time.end();
        
	}
	
	

}
