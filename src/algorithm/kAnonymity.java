package algorithm;

public class kAnonymity extends Anonymity {

	private int K; 
	
	public kAnonymity(int k){
		this.K =k;
	}

	public int getK() {
		return K;
	}

	public void setK(int k) {
		K = k;
	}
	
	
}
