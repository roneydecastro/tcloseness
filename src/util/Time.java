package util;


import java.util.Calendar;
import java.util.Date;

public class Time {
	
	Calendar calendar;
	Date startDate;
	Date endDate;
	
	public Time(){
		calendar = Calendar.getInstance();
	}
	
	
	public void start(){
		startDate = calendar.getTime();
	}
	
	public void end(){
        calendar = Calendar.getInstance();
        endDate = calendar.getTime();
        long sumDate = endDate.getTime() - startDate.getTime();
        System.out.println("Execution time is " + sumDate + " seconds");
	}
}
