package util;

import java.util.ArrayList;
import java.util.List;

public class Data implements Cloneable {
	
	private List<String[]> data = new ArrayList<String[]>();

	public List<String[]> getData() {
		return data;
	}


	public void setData(List<String[]> data) {
		this.data = data;
	}
	
	public void add(String... row){
		data.add(row);
	}
	
	public Data getClone() { 
		try { 
			return (Data) super.clone(); 
		} catch (CloneNotSupportedException e) { 
			System.out.println (" Cloning not allowed. " );
			return this; 
		} 
	}

	

}
