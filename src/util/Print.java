package util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.deidentifier.arx.ARXLattice.ARXNode;
import org.deidentifier.arx.ARXResult;
import org.deidentifier.arx.DataHandle;

import algorithm.Keys;

public class Print {

	public static void print(DataHandle handle) {
        final Iterator<String[]> itHandle = handle.iterator();
        print(itHandle);
    }


    public static void print(Iterator<String[]> iterator) {
        while (iterator.hasNext()) {
            System.out.print("   ");
            System.out.println(Arrays.toString(iterator.next()));
        }
    }

    public static void printHash(Map<Keys,Integer> hash) {
        System.out.println("{");
        for (Keys keys: hash.keySet()){

            String key = keys.toString();
            int value = hash.get(keys);
            System.out.println("<" + key + "," + value + ">");  

        }
        System.out.println("}");
    }
    
    public static void print(Data data) {
        System.out.println("{");
        for (String[] instance: data.getData()){
        	String aux = "";
        	for(int i=0;i<instance.length;i++){
        		aux = aux.concat(instance[i] + "\t");
        	}
            System.out.println(aux);  
        }
        System.out.println("}");
    }

    public static void print(Hierarchy hierarchy) {
        System.out.println("{");
        for (String[] instance: hierarchy.getHierarchy()){
        	String aux = "";
        	for(int i=0;i<instance.length;i++){
        		aux = aux.concat(instance[i] + "\t");
        	}
            System.out.println(aux);  
        }
        System.out.println("}");
    }
    public static void printArray(String[][] array) {
        System.out.print("{");
        for (int j=0; j<array.length; j++){
            String[] next = array[j];
            System.out.print("{");
            for (int i = 0; i < next.length; i++) {
                String string = next[i];
                System.out.print("\"" + string + "\"");
                if (i < next.length - 1) {
                    System.out.print(",");
                }
            }
            System.out.print("}");
            if (j<array.length-1) {
                System.out.print(",\n");
            }
        }
        System.out.println("}");
    }


    public static void printHandle(DataHandle handle) {
        final Iterator<String[]> itHandle = handle.iterator();
        printIterator(itHandle);
    }

    public static void printIterator(Iterator<String[]> iterator) {
        System.out.print("{");
        while (iterator.hasNext()) {
            System.out.print("{");
            String[] next = iterator.next();
            for (int i = 0; i < next.length; i++) {
                String string = next[i];
                System.out.print("\"" + string + "\"");
                if (i < next.length - 1) {
                    System.out.print(",");
                }
            }
            System.out.print("}");
            if (iterator.hasNext()) {
                System.out.print(",");
            }
        }
        System.out.println("}");
    }
    

    public static void printResult(final ARXResult result, final org.deidentifier.arx.Data data) {

        // Print time
        final DecimalFormat df1 = new DecimalFormat("#####0.00");
        final String sTotal = df1.format(result.getTime() / 1000d) + "s";
        System.out.println(" - Time needed: " + sTotal);

        // Extract
        final ARXNode optimum = result.getGlobalOptimum();
        final List<String> qis = new ArrayList<String>(data.getDefinition().getQuasiIdentifyingAttributes());

        if (optimum == null) {
            System.out.println(" - Criteria cannot be enforced!");
            return;
        }

        // Initialize
        final StringBuffer[] identifiers = new StringBuffer[qis.size()];
        final StringBuffer[] generalizations = new StringBuffer[qis.size()];
        int lengthI = 0;
        int lengthG = 0;
        for (int i = 0; i < qis.size(); i++) {
            identifiers[i] = new StringBuffer();
            generalizations[i] = new StringBuffer();
            identifiers[i].append(qis.get(i));
            generalizations[i].append(optimum.getGeneralization(qis.get(i)));
            if (data.getDefinition().isHierarchyAvailable(qis.get(i)))
                generalizations[i].append("/").append(data.getDefinition().getHierarchy(qis.get(i))[0].length - 1);
            lengthI = Math.max(lengthI, identifiers[i].length());
            lengthG = Math.max(lengthG, generalizations[i].length());
        }

        // Padding
        for (int i = 0; i < qis.size(); i++) {
            while (identifiers[i].length() < lengthI) {
                identifiers[i].append(" ");
            }
            while (generalizations[i].length() < lengthG) {
                generalizations[i].insert(0, " ");
            }
        }

        // Print
        System.out.println(" - Information loss: " + result.getGlobalOptimum().getMaximumInformationLoss());
        System.out.println(" - Optimal generalization");
        for (int i = 0; i < qis.size(); i++) {
            System.out.println("   * " + identifiers[i] + ": " + generalizations[i]);
        }
    }
}
