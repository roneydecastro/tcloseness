package util;

import java.util.ArrayList;
import java.util.List;

public class Hierarchy {

	private List<String[]> hierarchy = new ArrayList<String[]>();

	public List<String[]> getHierarchy() {
		return hierarchy;
	}

	public void setHierarchy(List<String[]> hierarchy) {
		this.hierarchy = hierarchy;
	}
	 
	
	public void add(String... row){
		hierarchy.add(row);
	}
	 
	 
	 
}
