package util;

import java.util.HashMap;
import java.util.Map;

import algorithm.Anonymity;
import algorithm.Anonymizer;


public class Config {
	
	private Map<String, Hierarchy> definitions = new HashMap<String, Hierarchy>();
	private Anonymity anonymity;
	private Map<String, AttributeType> attibutesType = new HashMap<String, AttributeType>();
	


	public enum AttributeType{
		
		IDENTIFYING_ATTRIBUTE("IDENTIFYING_ATTRIBUTE"),
		SENSITIVE_ATTRIBUTE("SENSITIVE_ATTRIBUTE"),
		INSENSITIVE_ATTRIBUTE("INSENSITIVE_ATTRIBUTE"),
		QUASI_IDENTIFYING_ATTRIBUTE("QUASI_IDENTIFYING_ATTRIBUTE");
		
		private final String value; 
		
		AttributeType(String value){
			this.value = value; 
		} 
		public String getValue(){ 
			return this.value; 
		}

	}
	
	 public void setDefinitions(String attribute,Hierarchy hierarchy) {
		    definitions.put(attribute, hierarchy);
	 }
	 
	 
	 
	 public Map<String, Hierarchy> getDefinitions() {
		return definitions;
	}



	public void setDefinitions(Map<String, Hierarchy> definitions) {
		this.definitions = definitions;
	}



	public Anonymity getAnonymity() {
		return anonymity;
	}



	public void setAnonymity(Anonymity anonymity) {
		this.anonymity = anonymity;
	}



	public void addCriterion(Anonymity anonymity){
		 this.anonymity = anonymity;
	 }
	public Anonymity getCriterion(){
		 return this.anonymity;
	 }
	
	public void addAttibuteType(String key, AttributeType value){
		attibutesType.put(key, value);
	}
	public AttributeType getAttibuteType(String key){
		return attibutesType.get(key);
	}

}
